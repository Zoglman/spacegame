
#pragma once

#include "EnemyShip.h"


class BioEnemyShip : public EnemyShip
{

public:

	BioEnemyShip();
	virtual ~BioEnemyShip() { }

	void SetTexture(Texture *pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime *pGameTime);

	virtual void Draw(SpriteBatch *pSpriteBatch);

	virtual void AddGameObject(GameObject* pGameObject) { m_gameObjects.push_back(pGameObject); }

	std::vector<GameObject*>::iterator m_it = m_gameObjects.begin();

	


private:

	Texture *m_pTexture;
	BioEnemyShip* m_BioEnemyShip;
	std::vector<Projectile*> m_projectiles;

	std::vector<GameObject*> m_gameObjects;


};