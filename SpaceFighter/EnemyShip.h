
#pragma once

#include "Ship.h"

class EnemyShip : public Ship
{

public:

	EnemyShip();
	virtual ~EnemyShip() { }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch) = 0; // pure virtual function

	virtual void Initialize(const Vector2 position, const double delaySeconds);

	virtual void Fire() { }

	virtual void Hit(const float damage);

	virtual std::string ToString() const { return "Enemy Ship"; }

	virtual CollisionType GetCollisionType() const { return CollisionType::ENEMY | CollisionType::SHIP; }

	virtual void AddGameObject(GameObject* pGameObject) { m_gameObjects.push_back(pGameObject); }

	std::vector<GameObject*>::iterator m_it = m_gameObjects.begin();


protected:

	virtual double GetDelaySeconds() const { return m_delaySeconds; }


private:

	double m_delaySeconds;

	double m_activationSeconds;


	EnemyShip* m_EnemyShip;
	std::vector<Projectile*> m_projectiles;

	std::vector<GameObject*> m_gameObjects;
	


};
